const mongoose = require('mongoose'); 

let User_Schema = new mongoose.Schema({ 
    username: String, 
    password: String
}); 

User_Schema.methods.validPassword = (pwd) => { 
    return (this.password == pwd); 
}

User_Schema.pre('save', ((next) => { 
    console.log('Saving element into db...'); 
    next(); 
})); 

let User = mongoose.model('User', User_Schema); 


module.exports = User; 