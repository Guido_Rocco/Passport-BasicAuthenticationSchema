const app = require('express')(); 
const passport = require('passport'); 
const LocalStrategy = require('passport-local').Strategy; 
const mongoose = require('mongoose'); 
const body_parser = require('body-parser'); 
const morgan = require('morgan'); 
const User = require('./data/User.js'); 

const port = process.env.port || 3080; 

app.use(morgan('dev')); 
app.use(body_parser.urlencoded({extended: true})); 
app.use(body_parser.json()); 

app.use(passport.initialize()); 
app.use(passport.session()); 
app.use(require('express-session')({ 
    secret: 'keyboard cat', resave: true, saveUninitialized: true }));


passport.serializeUser((user, done) => { 
    console.log('Serializing user...'); 
    done(null, user); 
}); 

passport.deserializeUser((user, done) => { 
    console.log('Deserializing user...'); 
    done(null, user); 
}); 

function isAuthenticated(req, res, next) { 
   if(req.isAuthenticated) { 
       next(); 
   } 
   else { 
       res.redirect('/login'); 
   }
} 

//Connects the MongoDB database 
mongoose.connect('mongodb://127.0.0.1:27017/my_database'); 
let db = mongoose.connection; 

db.once('open', () => { 
    console.log('Server successfully connected to database.'); 
}); 

db.on('error', () => { 
    console.error.bind(console, 'MongoDB connection error'); 
}); 


//Defines the Local Authentication Strategy 
passport.use(new LocalStrategy((username, password, done) => { 
    
   User.findOne({'username': username}, (err, user) => { 
       if(err) { 
           return done(err); 
       } 
       if(!user) { 
           return done(null, false, {message: 'Incorrect username.'}); 
       } 
       if(user.password !== password) { 
           return done(null, false, {message: 'Incorrect password. '}); 
       } 
       return done(null, user); 
   }); 
})); 


//Defines the protected APIs 
app.get('/index', isAuthenticated, 
(req, res, next) => { 
    res.send('Client is authenticated'); 
}); 


app.get('/about', isAuthenticated, 
(req, res, next) => { 
    res.send('You are now on the about page.'); 
}); 


//This API gets called if the user is not authenticated 
app.get('/not_logged_in', (req, res) => { 
    console.log(req.session); 
    res.send('You are not logged in. Please re do the authentication process'); 
}); 


//This API gets called if the user is autenticated 
app.get('/ok', (req, res) => { 
    res.send('OK. User successfully authenticated.'); 
}); 


//The Login API 
app.post('/login', passport.authenticate('local', { 
    successRedirect: '/ok', 
    failureRedirect: '/login'
}), (req, res) => { 
   res.status(200).send(); 
}); 


app.get('/login', (req, res) => { 
    res.send('Please login to access protected APIs. '); 
}); 


//Registers a new user into the MongoDB 
app.post('/register', (req, res, next) => { 

    var new_user = new User({ 
        username: req.body.username, 
        password: req.body.password
    }); 

    console.log(req.body); 
    console.log(new_user); 

    User.create(new_user, (err, user) => { 
        if(err)  { 
            return next(err); 
        } 
        console.log('Element successfully saved into database'); 
        res.status(200).send(); 
    }); 
}); 


//Initializes the server 
app.listen(port, (err) => { 
    if(err) { 
        console.log('Error while initializing server: ', err); 
        throw err; 
    } 
    console.log(`Server listening on port ${port}`); 
}); 


